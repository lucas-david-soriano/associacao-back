
module.exports       = app => {
   const router      = app.routerGroup()
   const categorias  = app.controllers.categorias
   const {auth}      = app.middlewares.auth

   router.get('/categorias', categorias.findAll)
   router.get('/categorias/:associacao_id', categorias.associacao)
   router.post('/categorias', auth, categorias.create)
   router.put('/categorias', auth, categorias.edit)
   router.delete('/categorias/:id', auth, categorias.remove)

   return router
}
