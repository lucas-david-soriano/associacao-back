
module.exports     = app => {
   const router    = app.routerGroup()
   const cidades   = app.controllers.cidades
   const {auth}    = app.middlewares.auth

   router.get('/cidades', cidades.findAll)
   router.post('/cidades', auth, cidades.create)

   router.post('/cidades/addimage', auth, cidades.addImage)
   router.put('/cidades/removeimage', auth, cidades.removeImage)

   router.put('/cidades', auth, cidades.edit)
   router.delete('/cidades/:id', auth, cidades.remove)

   return router
}
