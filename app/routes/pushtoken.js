
module.exports     = app => {
   const pushtoken = app.controllers.pushtoken
   const {auth}    = app.middlewares.auth

   app.get('/pushtoken', auth, pushtoken.findAll)
   app.post('/pushtoken', auth, pushtoken.create)
   app.delete('/pushtoken/:id', auth, pushtoken.remove)
}
