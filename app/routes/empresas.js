module.exports     = app => {
   const router    = app.routerGroup()
   const empresas  = app.controllers.empresas
   const {auth}    = app.middlewares.auth

   router.get('/empresas', empresas.findAll)
   router.get('get/empresas/:empresa_id', empresas.get)
   router.get('/empresas/:associacao_id', empresas.associacao)
   router.get('/empresas-categoria/:categoria_id', empresas.getCategoria)
   router.post('/empresas', auth, empresas.create)
   router.put('/empresas', auth, empresas.edit)
   router.delete('/empresas/:id', auth, empresas.remove)

   router.post('/empresas/addimage', auth, empresas.addImage)
   router.put('/empresas/removeimage', auth, empresas.removeImage)

   return router
}
