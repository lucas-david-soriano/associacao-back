
module.exports    = app => {
   const router   = app.routerGroup()
   const noticias = app.controllers.noticias
   const {auth}   = app.middlewares.auth

   router.get('/noticias/:associacao_id', noticias.findAll)
   router.post('/noticias', auth, noticias.create)
   router.put('/noticias', auth, noticias.edit)
   router.delete('/noticias/:id', auth, noticias.remove)

   router.post('/noticias/addimage', auth, noticias.addImage)
   router.put('/noticias/removeimage', auth, noticias.removeImage)

   return router
}
