
module.exports        = app => {
   const router       = app.routerGroup()
   const associacoes  = app.controllers.associacoes
   const {auth}       = app.middlewares.auth

   router.get('/associacoes', auth, associacoes.findAll)
   router.get('/associacoes/:associacao_id', associacoes.get)
   router.get('/associacoes/cidade/:cidade_id', auth, associacoes.cidade)
   router.post('/associacoes', auth, associacoes.create)
   router.put('/associacoes', auth, associacoes.edit)
   router.delete('/associacoes/:id', auth, associacoes.remove)

   router.post('/associacoes/addimage', auth, associacoes.addImage)
   router.put('/associacoes/removeimage', auth, associacoes.removeImage)

   return router
}
