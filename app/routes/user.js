module.exports   = app => {
    const router = app.routerGroup()
    const user   = app.controllers.user
    const {auth} = app.middlewares.auth

    router.get('/user', user.findAll)
    router.get('/user/associacao/:associacao_id', auth, user.associacao)
    router.post('/user', user.create)
    router.put('/user', auth, user.edit)
    router.delete('/user/:id', auth, user.remove)

    return router
}