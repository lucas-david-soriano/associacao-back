
const Resolver = promise => promise.then(result => [null, result]).catch(error => [error])

module.exports = app => {
    const empresasModel    = app.models.empresas
    const categoriasModel  = app.models.categorias
    const controller       = {}
    const hideFields       = {enabled:false,createdAt:false,updatedAt:false}

    controller.findAll = async (req, res) => {
      const [error, categorias] = await Resolver( categoriasModel.find({enabled:true},hideFields) )
      if (error) return res.json({ status: false, message: error.message.split(":").pop() })
      res.json(categorias)
    }

    controller.associacao     = async (req, res) => {
      const [error, empresas] = await Resolver( empresasModel.find({"associacao_id": req.params.associacao_id}, {categoria_id:true}).populate('categoria_id').exec() )
      if (error) return res.json({ status: false, message: error.message.split(":").pop() })
      res.json(empresas)
    }

    controller.create = async (req, res) => {
      const [error, dados] = await Resolver( categoriasModel.create(req.body) )
      if (error) return res.json({ status: false, message: error.message.split(":").pop() })
      res.json({ status: true, message: "Categoria cadastrada com sucesso" })
    }

    controller.edit = async (req, res) => {
      const [error, dados] = await Resolver( categoriasModel.updateOne({"_id":req.body._id}, {$set: req.body}) )
      if (error) return res.json({ status: false, message: error.message.split(":").pop() })
      res.json({ status: true, message: "Categoria alterada com sucesso" })
    }

    controller.remove = async (req, res) => {
      const [error, dados] = await Resolver( categoriasModel.updateOne({"_id":req.params.id}, {$set:{enabled:false}}) )
      if (error) return res.json({ status: false, message: error.message.split(":").pop() })
      res.json({ status: true, message: "Categoria removida com sucesso" })
    }

    return controller
}
