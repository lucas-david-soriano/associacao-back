const jwt            = require('jsonwebtoken')
const config         = require('../../config/config.json')
const passwordHash   = require('password-hash')

module.exports       = app => {
    const userModel  = app.models.user
    const controller = {}

    controller.login = async (req, res) => {
        let {email, senha} = req.body

        try{
            let u = await userModel.findOne({email, enabled:true},{createdAt:false,updatedAt:false}).populate('associacao_id').exec()

            if( !u || !passwordHash.verify( senha, u.senha ) ) 
                return res.json({status:false, message:"Usuário ou e-mail inválido"})
            
            if (!u.isRoot) {
                if( !u.associacao_id.enabled ) 
                return res.json({status:false, message:"Associação desabilitada"})
            }
            

            let usuario = {...u._doc}
            delete usuario.senha
            delete usuario.enabled
            jwt.sign({usuario}, config.secret, { expiresIn: config.expiresIn }, (err, token) => {
                return res.json({status: true, usuario, token: 'bearer '+ token })
            })

        } catch (err){
            console.log(err)
            res.status(500).json({status: false, err})
        }
    }

    return controller
}