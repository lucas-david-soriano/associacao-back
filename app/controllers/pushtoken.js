
const Resolver = promise => promise.then(result => [null, result]).catch(error => [error])
module.exports = app => {
    const pushtokenModel  = app.models.pushtoken
    const controller      = {}

    controller.findAll = async (req, res) => {
      const [error, dados] = await Resolver( pushtokenModel.find({}) )
      if (error) return res.json(error)
      res.json(dados)
    }

    controller.create = async (req, res) => {
      const [error, dados] = await Resolver( pushtokenModel.create(req.body) )
      if (error) return res.json(error)
      res.json(dados)
    }

    controller.remove = async (req, res) => {
      const [error, dados] = await Resolver( pushtokenModel.updateOne({"_id":req.params.id}, {$set: {enabled:false} }) )
      if (error) return res.json(error)
      res.json(dados)
    }

    return controller
}
