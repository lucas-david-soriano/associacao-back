const IMGS     = __dirname.replace("controllers", 'uploads/imgs/associacoes/')
const Resolver = promise => promise.then(result => [null, result]).catch(error => [error])
const fs       = require("fs")

module.exports = app => {
    const associacoesModel = app.models.associacoes
    const controller       = {}
    const hideFields       = {enabled:false,createdAt:false,updatedAt:false}

    controller.findAll     = async (req, res) => {
      const [error, dados] = await Resolver( associacoesModel.find({enabled:true},hideFields).populate('cidade_id',hideFields).exec() )
      if (error) return res.json({ status: false, message: error.message.split(":").pop() })
      res.json(dados)
    }
    
    controller.get = async (req, res) => {
      const [error, associacao] = await Resolver( associacoesModel.findOne({"_id": req.params.associacao_id},hideFields).populate('cidade_id',hideFields).exec() )
      if (error) return res.json({ status: false, message: error.message.split(":").pop() })
      res.json(associacao)
    }

    controller.cidade = async (req, res) => {
      const [error, associacao] = await Resolver( associacoesModel.find({"cidade_id": req.params.cidade_id},hideFields).populate('cidade_id',hideFields).exec() )
      if (error) return res.json({ status: false, message: error.message.split(":").pop() })
      res.json(associacao)
    }

    controller.create      = async (req, res) => {
      for(let base64 in req.body.imagens)
      {
        let [format, base] = req.body.imagens[base64].split(',')
        format             = format.replace('data:image/', '').replace(';base64', '')
        req.body.imagens[base64] = `${new Date().getTime()}.${format}`
        fs.writeFileSync( `${IMGS}${req.body.imagens[base64]}`, base, 'base64')
      }

      const [error, dados] = await Resolver( associacoesModel.create(req.body) )
      if (error) return res.json({ status: false, message: error.message.split(":").pop() })
      res.json({ status: true, message: "Associação cadastrada com sucesso" })
    }

    /*
      {
        associacao_id
        imagem
      }
    */
    controller.addImage = async (req, res) => {
      let [format, base] = req.body.imagem.split(',')
      format             = format.replace('data:image/', '').replace(';base64', '')
      let nome           = `${new Date().getTime()}.${format}`
      fs.writeFileSync( `${IMGS}${nome}`, base, 'base64')

      const [error, dados] = await Resolver( associacoesModel.updateOne({"_id":req.body.associacao_id}, {$push: {imagens: nome } }) )
      if (error) return res.json({ status: false, message: error.message.split(":").pop() })
      res.json({ status: true, message: "Imagem adicionada com sucesso" })
    }

    /*
      {
        associacao_id
        imagem
      }
    */
    controller.removeImage = async (req, res) => {
      const [error, dados] = await Resolver( associacoesModel.updateOne({"_id":req.body.associacao_id}, {$pull: {imagens: req.body.imagem } }) )
      if (error) return res.json({ status: false, message: error.message.split(":").pop() })
      res.json({ status: true, message: "Imagem removida com sucesso" })
    }

    controller.edit        = async (req, res) => {
      delete req.body.imagens
      const [error, dados] = await Resolver( associacoesModel.updateOne({"_id":req.body._id}, {$set: req.body}) )
      if (error) return res.json({ status: false, message: error.message.split(":").pop() })
      res.json({ status: true, message: "Associação alterada com sucesso" })
    }

    controller.remove      = async (req, res) => {
      const [error, dados] = await Resolver( associacoesModel.updateOne({"_id":req.params.id}, {$set: {enabled:false}}) )
      if (error) return res.json({ status: false, message: error.message.split(":").pop() })
      res.json({ status: true, message: "Associação removida com sucesso" })
    }

    return controller
}
