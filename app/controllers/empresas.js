const IMGS     = __dirname.replace("controllers", 'uploads/imgs/empresas/')
const Resolver = promise => promise.then(result => [null, result]).catch(error => [error])
const fs       = require("fs")

module.exports = app => {
    const empresasModel  = app.models.empresas
    const controller     = {}
    const hideFields     = {enabled:false,createdAt:false,updatedAt:false}

    controller.findAll = async (req, res) => {
      const [error, empresas] = await Resolver( empresasModel.find({enabled:true},hideFields).populate('associacao_id').populate('categoria_id').exec() )
      if (error) return res.json({ status: false, message: error.message.split(":").pop() })
      res.json(empresas)
    }

    controller.get = async (req, res) => {
      const [error, empresas] = await Resolver( empresasModel.findOne({"_id": req.params.empresa_id},hideFields).populate('associacao_id').populate('categoria_id').exec() )
      if (error) return res.json({ status: false, message: error.message.split(":").pop() })
      res.json(empresas)
    }


    controller.getCategoria = async (req, res) => {
      const [error, empresas] = await Resolver( empresasModel.find({categoria_id: req.params.categoria_id},hideFields).populate('associacao_id').populate('categoria_id').exec() )
      if (error) return res.json({ status: false, message: error.message.split(":").pop() })
      res.json(empresas)
    }

    controller.associacao = async (req, res) => {
      const [error, empresas] = await Resolver( empresasModel.find({"associacao_id": req.params.associacao_id},hideFields).populate('associacao_id').populate('categoria_id').exec() )
      if (error) return res.json({ status: false, message: error.message.split(":").pop() })
      res.json(empresas)
    }

    controller.create = async (req, res) => {
      for(let base64 in req.body.imagens)
      {
        let [format, base] = req.body.imagens[base64].split(',')
        format             = format.replace('data:image/', '').replace(';base64', '')
        req.body.imagens[base64] = `${new Date().getTime()}.${format}`
        fs.writeFileSync( `${IMGS}${req.body.imagens[base64]}`, base, 'base64')
      }

      const [error, dados] = await Resolver( empresasModel.create(req.body) )
      if (error) return res.json({ status: false, message: error.message.split(":").pop() })
      res.json({ status: true, message: "Empresa cadastrada com sucesso" })
    }


    /*
      {
        empresa_id
        imagem
      }
    */
    controller.addImage = async (req, res) => {
      let [format, base] = req.body.imagem.split(',')
      format             = format.replace('data:image/', '').replace(';base64', '')
      let nome           = `${new Date().getTime()}.${format}`
      fs.writeFileSync( `${IMGS}${nome}`, base, 'base64')

      const [error, dados] = await Resolver( empresasModel.updateOne({"_id":req.body.empresa_id}, {$push: {imagens: nome } }) )
      if (error) return res.json({ status: false, message: error.message.split(":").pop() })
      res.json({ status: true, message: "Imagem adicionada com sucesso" })
    }

    /*
      {
        empresa_id
        imagem
      }
    */
    controller.removeImage = async (req, res) => {
      const [error, dados] = await Resolver( empresasModel.updateOne({"_id":req.body.empresa_id}, {$pull: {imagens: req.body.imagem } }) )
      if (error) return res.json({ status: false, message: error.message.split(":").pop() })
      res.json({ status: true, message: "Imagem removida com sucesso" })
    }


    controller.edit = async (req, res) => {
      const [error, dados] = await Resolver( empresasModel.updateOne({"_id":req.body._id}, {$set: req.body}) )
      if (error) return res.json({ status: false, message: error.message.split(":").pop() })
      res.json({ status: true, message: "Empresa alterada com sucesso" })
    }

    controller.remove = async (req, res) => {
      const [error, dados] = await Resolver( empresasModel.updateOne({"_id":req.params.id}, {$set:{enabled:false}}) )
      if (error) return res.json({ status: false, message: error.message.split(":").pop() })
      res.json({ status: true, message: "Empresa removida com sucesso" })
    }

    return controller
}
