const passwordHash = require('password-hash')
const Resolver     = promise => promise.then(result => [null, result]).catch(error => [error])

module.exports = app => {
    const userModel  = app.models.user
    const controller = {}
    const hideFields = {senha:false,enabled:false,createdAt:false,updatedAt:false}

    controller.findAll = async (req, res) => {
        const [error, usuarios] = await Resolver(userModel.find({enabled:true},hideFields).populate('associacao_id').exec() )
        if (error) return res.json({ status: false, message: error.message.split(":").pop() })
        res.json(usuarios)
    }

    controller.associacao   = async (req, res) => {
        const [error, user] = await Resolver( userModel.find({},hideFields) )
        if (error) return res.json({ status: false, message: error.message.split(":").pop() })
        res.json(user)
    }

    controller.create  = async (req, res) => {
        req.body.senha = passwordHash.generate( req.body.senha )
        const [error, result] = await Resolver(userModel.create(req.body))
        if (error) return res.json({ status: false, message: error.code === 11000 ? 'E-mail indisponível' : error.message.split(":").pop() })
        res.json({ status: true, message: "Usuário cadastrado com sucesso" })
    }

    controller.edit = async (req, res) => {
        if(!req.body.senha)
            delete req.body.senha
        else 
            req.body.senha = passwordHash.generate( req.body.senha )

        const [error, result] = await Resolver(userModel.updateOne({"_id":req.body._id}, {$set: req.body}))
        if (error) return res.json({ status: false, message: error.message.split(":").pop() })
        res.json({ status: true, message: "Usuário alterado com sucesso" })
    }

    controller.remove    = async (req, res) => {
        const [error, u] = await Resolver(userModel.updateOne({"_id":req.params.id},{$set:{enabled:false}}))
        if (error) return res.json({ status: false, message: error.message.split(":").pop() })
        res.json({ status: true, message: "Usuário removido com sucesso" })
    }

    return controller
}
