
const mongoose  = require('mongoose')

module.exports  = app => {
   const schema = mongoose.Schema({
      titulo:{
         type:String,
         require:[true, 'Informe o titulo da notícia']
      },
      descricao:{
         type:String,
         require:[true, 'Descreva a notícia']
      },
      imagens:{
         type:[String]
      },
      associacao_id:{
         type: mongoose.Schema.ObjectId,
         ref: 'associacoes', 
         required : [true, 'Empresa é obrigatória'] 
      },
      enabled: {
         type: Boolean,
         default: true
      }
   },{versionKey: false})
   schema.set('timestamps', true)

   return mongoose.model('noticias', schema)
}
