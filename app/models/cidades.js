
const mongoose  = require('mongoose')

module.exports  = app => {
   const schema = mongoose.Schema({
      nome:{
         type:String,
         require:[true, 'Informe o nome da cidade']
      },
      imagens:[String],
      descricao:{
         type:String,
         require:[true, 'Descreva a cidade']
      },
      enabled:{
         type:Boolean,
         default:true
      }
   },{versionKey: false})
   schema.set('timestamps', true)
   
   return mongoose.model('cidades', schema)
}
