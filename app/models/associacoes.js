
const mongoose  = require('mongoose')

module.exports  = app => {
   const schema = mongoose.Schema({
      site_link:{
         type:String,
         default:''
      },
      facebook_link:{
         type:String,
         default:'' 
      },
      youtube_link:{
         type:String,
         default:''
      },
      telefones:{
         type: [String]
      },
      email:{
         type: String,
         required: true,
         index: { unique: true },
         dropDups: true,
         validate: [ v => /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test(v), 'E-mail inválido']
      },
      location:{
         latitude:Number,
         longitude:Number
      },
      nome:{
         type:String,
         require: [true, 'Informe o nome']
      },
      descricao:{
         type:String,
         require: [true, 'Descreva a associação']
      },
      imagens:[String],
      cidade_id:{
         type: mongoose.Schema.ObjectId,
         ref: 'cidades', 
         required : [true, 'Associação é obrigatória'] 
     },
     enabled:{
        type:Boolean,
        default:true
     }
   },{versionKey: false})
   schema.set('timestamps', true)
   
   return mongoose.model('associacoes', schema)
}
