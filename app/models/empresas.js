
const mongoose  = require('mongoose')

module.exports  = app => {
   const schema = mongoose.Schema({
      nome:{
         type:String,
         require:[true, 'Informe o nome da empresa']
      },
      endereco: {
         type: Object,
         required: [true, 'Endereço é obrigatório'],
      },
      telefones: {
         type: Array
      },
      site_link:{
         type:String,
         default:''
      },
      facebook_link:{
         type:String,
         default:'' 
      },
      formas_pagamento:{
         type:Array
      },
      horario_inicio:{
         type:String,
         default:''
      },
      horario_fim:{
         type:String,
         default:''
      },
      imagens:[String],
      categoria_id:{
         type: mongoose.Schema.ObjectId,
         ref: 'categorias', 
         required : [true, 'Informe o seguimento da empresa'] 
      },
      associacao_id:{
         type: mongoose.Schema.ObjectId,
         ref: 'associacoes', 
         required : [true, 'Associação é obrigatória'] 
      },
      enabled: {
         type: Boolean,
         default:true
      }
   },{versionKey: false})
   schema.set('timestamps', true)

   return mongoose.model('empresas', schema)
}
