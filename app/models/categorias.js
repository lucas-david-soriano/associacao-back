
const mongoose  = require('mongoose')

module.exports  = app => {
   const schema = mongoose.Schema({
      nome:{
         type:String,
         require: [true, 'Informe um nome para categoria']
      },
      icon:{
         type:String,
         require: [true, 'Informe um ícone para categoria']
      },
      /*associacao_id:{
         type: mongoose.Schema.ObjectId,
         ref: 'associacoes', 
         required : [true, 'Associação é obrigatória'] 
      },*/
      enabled:{
         type:Boolean,
         default:true
      }
   },{versionKey: false})
   schema.set('timestamps', true)

   return mongoose.model('categorias', schema)
}
