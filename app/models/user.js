const mongoose   = require('mongoose')

module.exports   = () => {
    const schema = mongoose.Schema({
        nome:{
            type: String,
            require:[true, 'Nome é obrigatório'] 
        },
        email: {
            type: String,
            required: true,
            index: { unique: true },
            dropDups: true,
            validate: [ v => /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test(v), 'E-mail inválido']
        },
        senha: {
            type: String,
            required: [true, 'Senha é obrigatória'] 
        },
        associacao_id:{
            type: mongoose.Schema.ObjectId,
            ref: 'associacoes', 
            required : [true, 'Associação é obrigatória'] 
        },
        enabled:{
            type:Boolean,
            default:true
        },
        isRoot:{
            type: Boolean,
            default: false
        }
    },{versionKey: false})
    schema.set('timestamps', true)
    
    return mongoose.model('users', schema)
}