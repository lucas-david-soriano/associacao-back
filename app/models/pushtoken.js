
const mongoose  = require('mongoose')

module.exports  = app => {
   const schema = mongoose.Schema({
      empresa_id:{
         type: mongoose.SchemaTypes.ObjectId,
         required: [true, 'Informe o empresa_id']
      },
      token:{
         type: String,
         required: [true, 'Informe o token']
      },
      enabled:{
         type:Boolean,
         default:true
      }
   },{versionKey: false})

   schema.set('timestamps', true)
   return mongoose.model('pushtoken', schema)
}
