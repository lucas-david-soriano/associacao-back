const load          = require('express-load')
const bodyParser    = require('body-parser')
const express       = require('express')
const config        = require('./config.json')

module.exports      = () => {
    const app       = express()
    app.routerGroup = express.Router
    app.use('/', express.static(__dirname + '/../app/views'))

    app.use('/associacoes', express.static(__dirname + '/../app/uploads/imgs/associacoes'))
    app.use('/cidades', express.static(__dirname + '/../app/uploads/imgs/cidades'))
    app.use('/noticias', express.static(__dirname + '/../app/uploads/imgs/noticias'))
    app.use('/empresas', express.static(__dirname + '/../app/uploads/imgs/empresas'))

    app.set('port', config.app_port)
    app.use(bodyParser.urlencoded({
        extended: true
    }))

    app.use(bodyParser.json({limit: '10mb', extended: true}))
    app.use(bodyParser.urlencoded({limit: '10mb', extended: true}))

    app.use(require('method-override')())

    app.use( (req, res, next) => {
        res.header('Access-Control-Allow-Origin', '*')
        res.header('Access-Control-Allow-Methods', 'DELETE, GET, POST, PUT, OPTIONS')
        res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization')
        res.header('Access-Control-Allow-Credentials', true)
        next()
    })

    load('models', { cwd: 'app'}).then('controllers').then('middlewares').then('routes').into(app)
    app.use('/v1', app.routes.associacoes)
    app.use('/v1', app.routes.categorias)
    app.use('/v1', app.routes.cidades)
    app.use('/v1', app.routes.empresas)
    app.use('/v1', app.routes.login)
    app.use('/v1', app.routes.noticias)
    app.use('/v1', app.routes.user)
    return app
}
